package com.igti.apialuno.entity;

import com.igti.apialuno.dto.FilmeDto;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Data
public class Filme {
    @Id
    private String id;
    private String titulo;
    private String diretor;
    private String genero;
    private int ano;
    
    public Filme() {
    }
    
    public Filme(FilmeDto filmeDto) {
        this.titulo = filmeDto.getTitulo();
        this.diretor = filmeDto.getDiretor();
        this.genero = filmeDto.getGenero();
        this.ano = filmeDto.getAno();
    }
}
