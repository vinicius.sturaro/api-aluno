package com.igti.apialuno.dto;

import lombok.Data;

@Data
public class FilmeDto {
    private String titulo;
    private String diretor;
    private String genero;
    private int ano;
}
