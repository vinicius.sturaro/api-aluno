package com.igti.apialuno.controller;

import com.igti.apialuno.entity.Filme;
import com.igti.apialuno.dto.FilmeDto;
import com.igti.apialuno.repository.FilmeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/")
public class PrincipalController {
    
    @Autowired
    FilmeRepository filmeRepository;
    
    @RequestMapping(method = RequestMethod.GET)
    public String saudacao() {
        return "Olá, mundo!";
    }
    
    @GetMapping("todos")
    public ResponseEntity<List<Filme>> listar() {
        List<Filme> filmes = filmeRepository.findAll();
        return new ResponseEntity<>(filmes, HttpStatus.OK);
    }
    
    @PostMapping
    public ResponseEntity<Void> criar(@RequestBody @Valid FilmeDto filmeDto) {
        Filme filme = new Filme(filmeDto);
        filmeRepository.save(filme);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
    
}
