package com.igti.apialuno.repository;


import com.igti.apialuno.entity.Filme;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface FilmeRepository extends MongoRepository<Filme, String> {
}
